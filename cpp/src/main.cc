#include "Character.h"
#include "Item.h"

int main(int argc, char *argv[])
{
    // Instancia 3 objetos Character
    Character ashe("Ashe", 15, 35, 40, 10, 6);
    Character morgana("Morgana", 10, 35, 35, 20, 6);
    Character jinx("Jinx", 35, 10, 40, 15, 6);

    // Instancia itens para os personagens
    Item *luva_couro = new Item("Luvas de Couro", 0.0, 5, 8);
    Item *bota_couro = new Item("Botas de Couro", 0.0, 5, 7);
    Item *espada_couro = new Item("Espada de Couro", 0.0, 10, 2);
    Item *elmo_couro = new Item("Elmo de Couro", 0.0, 5, 10);

    Item *luva_bronze = new Item("Luvas de Bronze", 0.0, 7, 10);
    Item *bota_bronze = new Item("Botas de Bronze", 0.0, 7, 10);
    Item *espada_bronze = new Item("Espada de Bronze", 0.0, 12, 3);
    Item *elmo_bronze = new Item("Elmo de Bronze", 0.0, 5, 12);
        
    Item *luva_ferro = new Item("Luva de Ferro", 0.0, 6, 16);
    Item *bota_ferro = new Item("Bota de Ferro", 0.0, 6, 14);
    Item *espada_ferro = new Item("Espada de Ferro", 0.0, 14, 4);
    Item *elmo_ferro = new Item("Elmo de Ferro", 0.0, 6, 18);
       
    // Equipa os personagens com os itens
    ashe.equipItem(luva_couro);
    ashe.equipItem(bota_couro);
    ashe.equipItem(espada_couro);
    ashe.equipItem(elmo_couro);

    morgana.equipItem(luva_bronze);
    morgana.equipItem(bota_bronze);
    morgana.equipItem(espada_bronze);
    morgana.equipItem(elmo_bronze);
        
    jinx.equipItem(luva_ferro);
    jinx.equipItem(bota_ferro);
    jinx.equipItem(espada_ferro);
    jinx.equipItem(elmo_ferro);

    // Os personagens se atacam, exibindo mensagens que deixam claro o que houve
    ashe.attackCharacter(&morgana);
    morgana.attackCharacter(&jinx);
    jinx.attackCharacter(&ashe);

    return 0;
}
