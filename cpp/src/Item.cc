#include <iostream>

#include "Item.h"

/*
 * Nome: Item (Construtor)
 * Descricao: Construtor que define os valores iniciais para os atributos da classe
 * Entrada: (std::string) nome do item, (double) preço, (int) pontos de ataque, (int) pontos de defesa
*/
Item::Item(std::string name, double price, int attackpts, int defensepts)
{
    this->name = name;
    this->price = price;

    // Verifica se os pontos de ataque estao no intervalo permitido
    if(attackpts >= 5 && attackpts <= 50)
    {
        this->attackpts = attackpts;
    }
    else
    {
        this->attackpts = 5; // Valor padrao
        std::cout << "Erro em Item::Item(std::string, double, int, int): attackpts deve estar entre 5 e 50" << std::endl;
    }

    // Verifica se os pontos de defesa estao no intervalo permitido
    if(defensepts >= 1 && defensepts <= 30)
    {
        this->defensepts = defensepts;
    }
    else
    {
        this->defensepts = 1; // Valor padrao
        std::cout << "Erro em Item::Item(std::string, double, int, int): defensepts deve estar entre 1 e 30" << std::endl;
    }
}

/*
 * Nome: getName
 * Descricao: Retorna o nome do item
 * Saida: (std::string) nome
*/
std::string Item::getName()
{
    return name;
}

/*
 * Nome: getAttackPts
 * Descricao: Retorna os pontos de ataque
 * Saida: (int) ataque
*/
int Item::getAttackPts()
{
    return attackpts;
}

/*
 * Nome: getDefensePts
 * Descricao: Retorna os pontos de defesa
 * Saida: (int) defesa
*/
int Item::getDefensePts()
{
    return defensepts;
}

/*
 * Nome: getPrice
 * Descricao: Retorna o preço do item
 * Saida: (double) preço
*/
double Item::getPrice()
{
    return price;
}
