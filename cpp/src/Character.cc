#include <cstdlib>
#include <iostream>

#include "Character.h"

/*
 * Nome: Character (Construtor)
 * Descricao: Construtor que inicia os atributos e recebe apenas alias
 * Entrada: (std::string) nome do personagem
*/
Character::Character(std::string alias)
{
    HP = 100; // Valor inicial de HP

    strength = speed = dexterity = constitution = 25; // Valores padroes que somam 100 para os atributos

    XP = 1; // Valor inicial de HP

    this->alias = alias;

    myitems.setSpaces(6); // Quantidade padrão de espaçoes para o inventario

    srand( time(NULL) ); // Inicia o seed da funcao random
}

/*
 * Nome: Character (Construtor)
 * Descricao: Construtor que inicia os atributos a partir de valores dados
 * Entrada: (std::string) nome do personagem, (int) strength, (int) speed,
 * (int) dexterity, (int) constitution, (int) espaco no inventario
*/
Character::Character(std::string alias, int strength, int speed, int dexterity, int constitution, int invSpace)
{
    HP = 100; // Valor inicial de HP

    this->strength = strength;
    this->speed = speed;
    this->dexterity = dexterity;
    this->constitution = constitution;

    if(strength + speed + dexterity + constitution != 100) // Se os atributos nao somarem 100
        strength = speed = dexterity = constitution = 25; // Entao atribui valor padroes

    if(strength < 1 || speed < 1 || dexterity < 1 || constitution < 1) // Se algum dos atributos for menor do que o minimo (1)
        strength = speed = dexterity = constitution = 25; // Entao atribui os valores padroes

    XP = 1; // Valor inicial de XP

    this->alias = alias;
    
    myitems.setSpaces(invSpace); // Atribui a quantidade de espacos no inventario

    srand( time(NULL) ); // Inicia o seed da funcao random
}

/*
 * Nome: getName
 * Descricao: Retorna o alias
 * Saida: (std::string) nome do personagem
*/
std::string Character::getName()
{
    return alias;
}

/*
 * Nome: getDefensePoints
 * Descricao: Retorna a quantidade de pontos de defesa
 * Saida: (int) Pontos de Defesa
*/
int Character::getDefensePoints()
{
    // Pontos de defesa dados pelos itens
    int item_def_pts = 0;

    // Soma os pontos de defesa de todos os itens
    for(int i = 0; i < myitems.getNumberOfItems(); i++)
        item_def_pts += myitems.searchItem(i)->getDefensePts();
    
    // Calcula e retorna os pontos de defesa
    return (int)( ( ( constitution*0.6 + dexterity*0.1 + speed*0.3) + item_def_pts ) * (XP/6) );
}

/*
 * Nome: getAttackPoints
 * Descricao: Retorna a quantidade de pontos de ataque
 * Saida: (int) Pontos de Ataque
*/
int Character::getAttackPoints()
{
    // Pontos de ataque dados pelos itens
    int item_att_pts = 0;

    // Soma os pontos de ataque de todos os itens
    for(int i = 0; i < myitems.getNumberOfItems(); i++)
        item_att_pts += myitems.searchItem(i)->getAttackPts();

    // Calcula e retorna os pontos de ataque
    return (int)( ( ( strength*0.6 + dexterity*0.4 ) + item_att_pts ) * (XP/2) );
}

/*
 * Nome: attackCharacter
 * Descricao: Ataca um outro personagem
 * Entrada: (Character*) Referencia a outro personagem
 * Saida: (void)
*/
void Character::attackCharacter(Character *character)
{
    double miss_chance = 0.1 / XP; // Chance de miss
    
    int damage = 0;

    if( (double)rand()/RAND_MAX <= miss_chance) // Se o personagem errou
    {
        std::cout << "MISS" << std::endl;
    }
    else // Se acertou o golpe
    {
        int rnd = (rand()%11) - 5; // Gera um int entre -5 e 5
            
        damage = (getAttackPoints() - character->getDefensePoints()) + rnd; // Calcula o dano
            
        // Se o dano for menor ou igual a zero, utiliza dano 1
        if(damage <= 0)
            damage = 1;

        double critical_chance = 0.02*(XP/2); // Chance de ataque critico

        // Se o personagem conseguiu um ataque critico
        if( (double)rand()/RAND_MAX <= critical_chance )
        {
            damage *= 2;
            std::cout << "CRITICO" << std::endl;
        }

        character->takeDamage(damage);
    }

    std::cout << alias << " causou " << damage << " de dano em " << character->getName() << std::endl;
    std::cout << "    " << character->getName() << " ficou com " << character->getHP() << " de HP." << std::endl;
}

/*
 * Nome: takeDamage
 * Descricao: Toma o dano na vida do personagem
 * Entrada: (int) dano a ser tomado
 * Saida: (void)
*/
void Character::takeDamage(int damage)
{
        HP -= damage;
        
        if(HP <= 0)
        {
            HP = 0;
            std::cout << "O personagem " << alias << " morreu." << std::endl;
        }
}

/*
 * Nome: getHP
 * Descricao: Retorna o HP
 * Saida: (int) HP
*/
int Character::getHP()
{
    return HP;
}

/*
 * Nome: addXP
 * Descricao: Adiciona XP
 * Entrada: (int) xp a ser adicionada
 * Saida: (void)
*/
void Character::addXP(int XP)
{
    if(XP < 100)
        this->XP += XP;
}

/*
 * Nome: setStrength
 * Descricao: Atribui um valor de força
 * Entrada: (int) força
 * Saida: (void)
*/
void Character::setStrength(int strength)
{
    if(strength >= 1 && strength < 100)
        this->strength = strength;
}

/*
 * Nome: setSpeed
 * Descricao: Atribui um valor de velocidade
 * Entrada: (int) velocidade
 * Saida: (void)
*/
void Character::setSpeed(int speed)
{
    if(speed >= 1 && speed < 100)
        this->speed = speed;
}

/*
 * Nome: setDexterity
 * Descricao: Atribui um valor de dextreza
 * Entrada: (int) dextreza
 * Saida: (void)
*/
void Character::setDexterity(int dexterity)
{
    if(dexterity >= 1 && dexterity < 100)
        this->dexterity = dexterity;
}

/*
 * Nome: setConstitution
 * Descricao: Atribui um valor de constituicao
 * Entrada: (int) constituicao
 * Saida: (void)
*/
void Character::setConstitution(int constitution)
{
    if(constitution >= 1 && constitution < 100)
        this->constitution = constitution;
}

/*
 * Nome: equipItem
 * Descricao: Coloca um item no inventario
 * Entrada: (Item*) item
 * Saida: (void)
*/
void Character::equipItem(Item *item)
{
    myitems.insertItem(item);
}

/*
 * Nome: unequipItem
 * Descricao: Tira um item no inventario
 * Entrada: (Item*) item
 * Saida: (void)
*/
void Character::unequipItem(Item *item)
{
    myitems.removeItem(item->getName());
}
