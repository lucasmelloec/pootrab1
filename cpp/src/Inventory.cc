#include <iostream>

#include "Inventory.h"

/*
 * Nome: Inventory (Construtor)
 * Descricao: Construtor que inicializa os atributos em 0 e sem items no vetor
*/
Inventory::Inventory()
{
    spaces = 0;
    gold = 0;
}

/*
 * Nome: ~Inventory (Destrutor)
 * Descricao: Destrutor que libera a memoria do vetor de itens
*/
Inventory::~Inventory()
{
    for(int i = 0; i < items.size(); i++)
        delete items.at(i);

    items.clear();
}

/*
 * Nome: getTotalGold
 * Descricao: Retorna a quantidade de ouro possuida pelo personagem
 * Saida: (double) quantia de ouro
*/
double Inventory::getTotalGold()
{
    return gold;
}

/*
 * Nome: getAvailableSpace
 * Descricao: Retorna quanto espaço vazio há no inventário
 * Saida: (int) espaco disponivel
*/
int Inventory::getAvailableSpace()
{
    return spaces - items.size();
}

/*
 * Nome: getNumberOfItems
 * Descricao: Retorna quantos itens existem no inventario
 * Saida: (int) quantia de itens
*/
int Inventory::getNumberOfItems()
{
    return items.size();
}

/*
 * Nome: spendGold
 * Descricao: Diminui a quantia de ouro do personagem
 * Entrada: (double) quantia a ser retirada
 * Saida: (void)
*/
void Inventory::spendGold(double amount)
{
    if(amount <= gold)
        gold -= amount;
    else
        std::cout << "Erro em Inventory::spendGold(double): o personagem nao possui ouro suficiente." << std::endl;
}

/*
 * Nome: earnGold
 * Descricao: Aumenta a quantia de ouro do personagem
 * Entrada: (double) quantia a ser acrescida
 * Saida: (void)
*/
void Inventory::earnGold(double amount)
{
    gold += amount;
}

/*
 * Nome: setSpaces
 * Descricao: Atribui uma quantidade de espacos no inventário
 * Entrada: (int) espaço disponivel
 * Saida: (void)
*/
void Inventory::setSpaces(int spaces)
{
    this->spaces = spaces;
}

/*
 * Nome: searchItem
 * Descricao: Retorna um item do inventario
 * Entrada: (std::tring) nome do item
 * Saida: (Item*) referencia ao item com aquele nome
*/
Item* Inventory::searchItem(std::string itemName)
{
    // Itera por todos os itens
    for(std::vector<Item*>::iterator it = items.begin(); it != items.end(); ++it)
        if( itemName.compare( (*it)->getName() ) == 0 ) // Se nome do item for o mesmo que esta sendo procurado
            return *it; // Retorna o item

    return NULL;    // Retorna NULL caso o item nao seja encontrado
}

/*
 * Nome: searchItem
 * Descricao: Retorna um item do inventario
 * Entrada: (int) Posicao do item
 * Saida: (Item*) referencia ao item com aquela posicao no vetor
*/
Item* Inventory::searchItem(int pos)
{
    // Se a posicao estao abaixo do limite permitido
    if(pos < items.size())
        return items.at(pos);
    
    return NULL; // Se a posicao do item ultrapassa o limite, retorna null
}

/*
 * Nome: insertItem
 * Descricao: Insere um item no inventário
 * Entrada: (Item*) referencia ao item
 * Saida: (void)
*/
void Inventory::insertItem(Item *item)
{
    // Apenas insere o item se houver espaco disponivel
    if(items.size() < spaces)
        items.push_back(item);
    else
        std::cout << "Erro em Inventory::insertItem: O personagem nao possui espaco disponivel no inventario." << std::endl;
}

/*
 * Nome: removeItem
 * Descricao: Remove um item do inventário
 * Entrada: (std::tring) nome do item
 * Saida: (void)
*/
void Inventory::removeItem(std::string itemName)
{
    // Itera por todos os itens
    for(std::vector<Item*>::iterator it = items.begin(); it != items.end(); ++it)
    {
        if( itemName.compare( (*it)->getName() ) == 0 ) // Se nome do item for o mesmo que esta sendo procurado
        {
            // Deleta o item
            delete *it;
            items.erase(it); // Tira do vetor
        }
    }
}

/*
 * Nome: removeItem
 * Descricao: Remove um item do inventário
 * Entrada: (int) posicao do item
 * Saida: (void)
*/
void Inventory::removeItem(int pos)
{
    // Verifica se a posicao esta abaixo do limite permitido
    if(pos < items.size())
    {
        delete items.at(pos);
        items.erase(items.begin() + pos);
    }
    else
    {
        std::cout << "Erro em Inventory::removeItem: A posicao nao correponde a um item valido." << std::endl;
    }
}
