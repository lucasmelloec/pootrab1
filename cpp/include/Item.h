#ifndef _ITEM_H_
#define _ITEM_H_

#include <string>

/*
 * Classe Item
 *     Representa um item no jogo
*/
class Item
{
    private:
    std::string name; // Nome do item
    double price; // Preço do item

    protected:
    int attackpts; // Pontos de ataque do item
    int defensepts; // Pontos de defesa do item

    public:

    /*
     * Nome: Item (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (std::string) nome do item, (double) preço, (int) pontos de ataque, (int) pontos de defesa
    */
    Item(std::string name, double price, int attackpts, int defensepts);

    /*
     * Nome: getName
     * Descricao: Retorna o nome do item
     * Saida: (std::string) nome
    */
    std::string getName();

    /*
     * Nome: getAttackPts
     * Descricao: Retorna os pontos de ataque
     * Saida: (int) ataque
    */
    int getAttackPts();

    /*
     * Nome: getDefensePts
     * Descricao: Retorna os pontos de defesa
     * Saida: (int) defesa
    */
    int getDefensePts();

    /*
     * Nome: getPrice
     * Descricao: Retorna o preço do item
     * Saida: (double) preço
    */
    double getPrice();
};

#endif // _ITEM_H_
