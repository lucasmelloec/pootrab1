#ifndef _INVENTORY_H_
#define _INVENTORY_H_

#include <string>
#include <vector>

#include "Item.h"

/*
 * Classe Inventory
 *     Inventario do personagem
*/
class Inventory
{
    private:
    int spaces; // Espaços do inventario
    double gold; // Ouro do personagem

    std::vector<Item*> items; // Itens presentes no inventario (colecao de objetos)

    public:

    /*
     * Nome: Inventory (Construtor)
     * Descricao: Construtor que inicializa os atributos em 0 e sem items no vetor
    */
    Inventory();

    /*
     * Nome: ~Inventory (Destrutor)
     * Descricao: Destrutor que libera a memoria do vetor de itens
    */
    ~Inventory();

    /*
     * Nome: getTotalGold
     * Descricao: Retorna a quantidade de ouro possuida pelo personagem
     * Saida: (double) quantia de ouro
    */
    double getTotalGold();

    /*
     * Nome: getAvailableSpace
     * Descricao: Retorna quanto espaço vazio há no inventário
     * Saida: (int) espaco disponivel
    */
    int getAvailableSpace();

    /*
     * Nome: getNumberOfItems
     * Descricao: Retorna quantos itens existem no inventario
     * Saida: (int) quantia de itens
    */
    int getNumberOfItems();

    /*
     * Nome: spendGold
     * Descricao: Diminui a quantia de ouro do personagem
     * Entrada: (double) quantia a ser retirada
     * Saida: (void)
    */
    void spendGold(double amount);

    /*
     * Nome: earnGold
     * Descricao: Aumenta a quantia de ouro do personagem
     * Entrada: (double) quantia a ser acrescida
     * Saida: (void)
    */
    void earnGold(double amount);

    /*
     * Nome: setSpaces
     * Descricao: Atribui uma quantidade de espacos no inventário
     * Entrada: (int) espaço disponivel
     * Saida: (void)
    */
    void setSpaces(int spaces);

    /*
     * Nome: searchItem
     * Descricao: Retorna um item do inventario
     * Entrada: (std::string) nome do item
     * Saida: (Item*) referencia ao item com aquele nome
    */
    Item* searchItem(std::string itemName);

    /*
     * Nome: searchItem
     * Descricao: Retorna um item do inventario
     * Entrada: (int) Posicao do item
     * Saida: (Item*) referencia ao item com aquela posicao no vetor
    */
    Item* searchItem(int pos);

    /*
     * Nome: insertItem
     * Descricao: Insere um item no inventário
     * Entrada: (Item*) referencia ao item
     * Saida: (void)
    */
    void insertItem(Item *item);

    /*
     * Nome: removeItem
     * Descricao: Remove um item do inventário
     * Entrada: (std::string) nome do item
     * Saida: (void)
    */
    void removeItem(std::string itemName);

    /*
     * Nome: removeItem
     * Descricao: Remove um item do inventário
     * Entrada: (int) Posicao do item
     * Saida: (void)
    */
    void removeItem(int pos);
};

#endif // _INVENTORY_H_
