#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>

#include "Inventory.h"

class Character
{
    private:

    std::string alias; // Nome do personagem

    Inventory myitems; // Inventario do personagem

    int HP; // Pontos de vida

    protected:
    
    int XP; // Experiencia
    int strength; // Força
    int speed; // Velocidade
    int dexterity; // Destreza
    int constitution; // Constituição

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    int getDefensePoints();

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    int getAttackPoints();

    public:

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos e recebe apenas alias
     * Entrada: (std::string) nome do personagem
    */
    Character(std::string alias);

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (std::string) nome do personagem, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    Character(std::string alias, int strength, int speed, int dexterity, int constitution, int invspace);

    /*
     * Nome: getName
     * Descricao: Retorna o alias
     * Saida: (string) nome do personagem
    */
    std::string getName();

    /*
     * Nome: attackCharacter
     * Descricao: Ataca um outro personagem
     * Entrada: (Character*) Referencia a outro personagem
     * Saida: (void)
    */
    void attackCharacter(Character *character);
    
    /*
     * Nome: addXP
     * Descricao: Adiciona XP
     * Entrada: (int) xp a ser adicionada
     * Saida: (void)
    */
    void addXP(int XP);

    /*
     * Nome: setStrength
     * Descricao: Atribui um valor de força
     * Entrada: (int) força
     * Saida: (void)
    */
    void setStrength(int strength);

    /*
     * Nome: setSpeed
     * Descricao: Atribui um valor de velocidade
     * Entrada: (int) velocidade
     * Saida: (void)
    */
    void setSpeed(int speed);

    /*
     * Nome: setDexterity
     * Descricao: Atribui um valor de dextreza
     * Entrada: (int) dextreza
     * Saida: (void)
    */
    void setDexterity(int dexterity);

    /*
     * Nome: setConstitution
     * Descricao: Atribui um valor de constituicao
     * Entrada: (int) constituicao
     * Saida: (void)
    */
    void setConstitution(int constitution);

    /*
     * Nome: takeDamage
     * Descricao: Toma o dano na vida do personagem
     * Entrada: (int) dano a ser tomado
     * Saida: (void)
    */
    void takeDamage(int damage);

    /*
     * Nome: getHP
     * Descricao: Retorna o HP
     * Saida: (int) HP
    */
    int getHP();
    
    /*
     * Nome: equipItem
     * Descricao: Coloca um item no inventario
     * Entrada: (Item*) item
     * Saida: (void)
    */
    void equipItem(Item *item);

    /*
     * Nome: unequipItem
     * Descricao: Tira um item no inventario
     * Entrada: (Item*) item
     * Saida: (void)
    */
    void unequipItem(Item *item);

};

#endif // _CHARACTER_H_
