package Items;

/*
 * Classe Item
 *  Representa um item no jogo
*/
public class Item
{
    private String name; // Nome do item
    private double price; // Preço do item

    protected int attackpts; // Pontos de ataque do item
    protected int defensepts; // Pontos de defesa do item

    /*
     * Nome: Item (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome do item, (double) preço, (int) pontos de ataque, (int) pontos de defesa
    */
    public Item(String name, double price, int attackpts, int defensepts)
    {
        this.name = name;
        this.price = price;

        // Se os pontos de ataque estiverem dentro do intervalo permitido
        if(attackpts >= 5 && attackpts <= 50)
        {
            this.attackpts = attackpts;
        }
        else
        {
            this.attackpts = 5; // Atribui valor padrao
            System.out.println("Erro em Item.Item(String, double, int, int): attackpts deve estar entre 5 e 50.");
        }

        // Se os pontos de defesa estiverem dentro do invervalo permitido
        if(defensepts >= 1 && defensepts <= 30)
        {
            this.defensepts = defensepts;
        }
        else
        {
            this.defensepts = 1; // Atribui valor padrao
            System.out.println("Erro em Item.Item(String, double, int, int): defensepts deve estar entre 1 e 30.");
        }
    }

    /*
     * Nome: getName
     * Descricao: Retorna o nome do item
     * Saida: (String) nome
    */
    public String getName()
    {
        return name;
    }

    /*
     * Nome: getAttackPts
     * Descricao: Retorna os pontos de ataque
     * Saida: (int) ataque
    */
    public int getAttackPts()
    {
        return attackpts;
    }

    /*
     * Nome: getDefensePts
     * Descricao: Retorna os pontos de defesa
     * Saida: (int) defesa
    */
    public int getDefensePts()
    {
        return defensepts;
    }

    /*
     * Nome: getPrice
     * Descricao: Retorna o preço do item
     * Saida: (double) preço
    */
    public double getPrice()
    {
        return price;
    }
}
