package Items;

import java.util.*;

/*
 * Classe Inventory
 *     Inventario do personagem
*/
public class Inventory
{
    private int spaces; // Espaços do inventario
    private double gold; // Ouro do personagem

    private ArrayList<Item> items; // Itens presentes no inventario (colecao de objetos)

    /*
     * Nome: Inventory (Construtor)
     * Descricao: Construtor que inicializa os atributos em 0 e sem items no vetor
    */
    public Inventory()
    {
        spaces = 0;
        gold = 0;

        items = new ArrayList<Item>();
    }

    /*
     * Nome: getTotalGold
     * Descricao: Retorna a quantidade de ouro possuida pelo personagem
     * Saida: (double) quantia de ouro
    */
    public double getTotalGold()
    {
        return gold;
    }

    /*
     * Nome: getAvailableSpace
     * Descricao: Retorna quanto espaço vazio há no inventário
     * Saida: (int) espaco disponivel
    */
    public int getAvailableSpace()
    {
        return spaces - items.size();
    }

    /*
     * Nome: getNumberOfItems
     * Descricao: Retorna quantos itens existem no inventario
     * Saida: (int) quantia de itens
    */
    public int getNumberOfItems()
    {
        return items.size();
    }

    /*
     * Nome: spendGold
     * Descricao: Diminui a quantia de ouro do personagem
     * Entrada: (double) quantia a ser retirada
     * Saida: (void)
    */
    public void spendGold(double amount)
    {
        if(amount <= gold)
            gold -= amount;
        else
            System.out.println("Erro em Inventory.spendGold(double): o personagem nao possui ouro suficiente.");
    }

    /*
     * Nome: earnGold
     * Descricao: Aumenta a quantia de ouro do personagem
     * Entrada: (double) quantia a ser acrescida
     * Saida: (void)
    */
    public void earnGold(double amount)
    {
        gold += amount;
    }

    /*
     * Nome: setSpaces
     * Descricao: Atribui uma quantidade de espacos no inventário
     * Entrada: (int) espaço disponivel
     * Saida: (void)
    */
    public void setSpaces(int spaces)
    {
        this.spaces = spaces;
    }

    /*
     * Nome: searchItem
     * Descricao: Retorna um item do inventario
     * Entrada: (String) nome do item
     * Saida: (Item) referencia ao item com aquele nome
    */
    public Item searchItem(String itemName)
    {
        // Itera por todos os itens
        for(Item it : items)
            if( it.getName().equals(itemName) ) // Se nome do item for o mesmo que esta sendo procurado
                return it; // Retorna o item

        return null;    // Retorna null caso o item nao seja encontrado
    }

    /*
     * Nome: searchItem
     * Descricao: Retorna um item do inventario
     * Entrada: (int) posicao do item
     * Saida: (Item) referencia ao item com aquela posicao no vetor
    */
    public Item searchItem(int pos)
    {
        // Se a posicao estiver abaixo do limite permitido
        if(pos < items.size())
            return items.get(pos);

        return null; // Se a posicao do item ultrapassa o limite, retorna null
    }

    /*
     * Nome: insertItem
     * Descricao: Insere um item no inventário
     * Entrada: (Item) referencia ao item
     * Saida: (void)
    */
    public void insertItem(Item item)
    {
        // Somente insere se houver espaco disponivel no inventario
        if(items.size() < spaces)
            items.add(item);
        else
            System.out.println("Erro em Inventory.insertItem: O personagem nao possui espaco disponivel no inventario.");
    }

    /*
     * Nome: removeItem
     * Descricao: Remove um item do inventário
     * Entrada: (String) nome do item
     * Saida: (void)
    */
    public void removeItem(String itemName)
    {
        // Itera por todos os itens
        for(Item it : items)
        {
            if( it.getName().equals(itemName) ) // Se nome do item for o mesmo que esta sendo procurado
            {
                // Tira o item do vetor
                items.remove(it); // Tira do vetor
            }
        }
    }

    /*
     * Nome: removeItem
     * Descricao: Remove um item do inventário
     * Entrada: (int) posicao do item
     * Saida: (void)
    */
    public void removeItem(int pos)
    {
        if(pos < items.size())
            items.remove(pos);
        else
            System.out.println("Erro em Inventory.removeItem: A posicao nao correponde a um item valido.");
    }
}
