import java.util.Random;

import Items.*;

public class Character
{
    private String alias; // Nome do personagem

    private Inventory myitems; // Inventario do personagem

    private int HP; // Pontos de vida

    private Random rand; // Objeto para gerar valores randomicos

    protected int XP; // Experiencia
    protected int strength; // Força
    protected int speed; // Velocidade
    protected int dexterity; // Destreza
    protected int constitution; // Constituição

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos e recebe apenas alias
     * Entrada: (String) nome do personagem
    */
    public Character(String alias)
    {
        HP = 100; // Valor inicial de HP

        strength = speed = dexterity = constitution = 25; // Valores padroes de atributos

        XP = 1; // Valor inicial de XP

        this.alias = alias;

        myitems = new Inventory();
        myitems.setSpaces(6); // Quantidade padrao de espacos no inventario

        rand = new Random(); // Instancia o objeto rand e atribui uma seed
    }

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (String) nome do personagem, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    public Character(String alias, int strength, int speed, int dexterity, int constitution, int invSpace)
    {
        HP = 100; // Valor inicial de HP

        this.strength = strength;
        this.speed = speed;
        this.dexterity = dexterity;
        this.constitution = constitution;

        if(strength + speed + dexterity + constitution != 100) // Se os atributos nao somarem 100
            strength = speed = dexterity = constitution = 25; // entao atribui os valores padroes

        if(strength < 1 || speed < 1 || dexterity < 1 || constitution < 1) // Se algum atributo tiver abaixo do valor minimo
            strength = speed = dexterity = constitution = 25; // entao atribui os valores padroes

        XP = 1; // Valor inicial de XP

        this.alias = alias;

        myitems = new Inventory();
        myitems.setSpaces(invSpace); // Atribui a quantidade de espacos do inventario

        rand = new Random(); // Instancia o objeto rand e atribui uma seed
    }

    /*
     * Nome: getName
     * Descricao: Retorna o alias
     * Saida: (String) nome do personagem
    */
    public String getName()
    {
        return alias;
    }

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    protected int getDefensePoints()
    {
        // Pontos de defesa dados pelos itens
        int item_def_pts = 0;

        // Soma todos os pontos de defesa dados pelos itens
        for(int i = 0; i < myitems.getNumberOfItems(); i++)
            item_def_pts += myitems.searchItem(i).getDefensePts();
        
        // Calcula e retorna os pontos de defesa
        Double def_pts =  ( ( constitution*0.6 + dexterity*0.1 + speed*0.3) + item_def_pts ) * (XP/6);
        return def_pts.intValue();
    }

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    protected int getAttackPoints()
    {
        // Pontos de ataque dados pelos itens
        int item_att_pts = 0;

        // Soma todos os pontos de ataque dados pelos itens
        for(int i = 0; i < myitems.getNumberOfItems(); i++)
            item_att_pts += myitems.searchItem(i).getAttackPts();

        // Calcula e retorna os pontos de ataque
        Double att_pts = ( ( strength*0.6 + dexterity*0.4 ) + item_att_pts ) * (XP/2);

        return att_pts.intValue();
    }

    /*
     * Nome: attackCharacter
     * Descricao: Ataca um outro personagem
     * Entrada: (Character) Referencia a outro personagem
     * Saida: (void)
    */
    public void attackCharacter(Character character)
    {
        double miss_chance = 0.1 / XP; // Chance de miss

        int damage = 0;

        if( rand.nextDouble() <= miss_chance) // Se o personagem errou
        {
            System.out.println("MISS");
        }
        else // Se acertou o golpe
        {
            int rnd = rand.nextInt(11) - 5; // Gera um int entre -5 e 5
            
            damage = (getAttackPoints() - character.getDefensePoints()) + rnd; // Calcula o dano
            
            // Se o dano for menor ou igual a zero, utiliza dano 1
            if(damage <= 0)
                damage = 1;

            double critical_chance = 0.02*(XP/2); // Chance de ataque critico

            // Se o personagem conseguiu um ataque critico
            if( rand.nextDouble() <= critical_chance )
            {
                damage *= 2;
                System.out.println("CRITICO");
            }

            character.takeDamage(damage);
        }

        System.out.println(alias + " causou " + damage + " de dano em " + character.getName());
        System.out.println("    " + character.getName() + " ficou com " + character.getHP() + " de HP.");
    }

    /*
     * Nome: takeDamage
     * Descricao: Toma o dano na vida do personagem
     * Entrada: (int) dano a ser tomado
     * Saida: (void)
    */
    public void takeDamage(int damage)
    {
        HP -= damage;
        
        if(HP <= 0)
        {
            HP = 0;
            System.out.println("O personagem " + alias + " morreu.");
        }
    }

    /*
     * Nome: getHP
     * Descricao: Retorna o HP
     * Saida: (int) HP
    */
    public int getHP()
    {
        return HP;
    }
    
    /*
     * Nome: addXP
     * Descricao: Adiciona XP
     * Entrada: (int) xp a ser adicionada
     * Saida: (void)
    */
    public void addXP(int XP)
    {
        if(XP < 100)
            this.XP += XP;
    }

    /*
     * Nome: setStrength
     * Descricao: Atribui um valor de força
     * Entrada: (int) força
     * Saida: (void)
    */
    public void setStrength(int strength)
    {
        if(strength >= 1 && strength < 100)
            this.strength = strength;
    }

    /*
     * Nome: setSpeed
     * Descricao: Atribui um valor de velocidade
     * Entrada: (int) velocidade
     * Saida: (void)
    */
    public void setSpeed(int speed)
    {
        if(speed >= 1 && speed < 100)
            this.speed = speed;
    }

    /*
     * Nome: setDexterity
     * Descricao: Atribui um valor de dextreza
     * Entrada: (int) dextreza
     * Saida: (void)
    */
    public void setDexterity(int dexterity)
    {
        if(dexterity >= 1 && dexterity < 100)
            this.dexterity = dexterity;
    }

    /*
     * Nome: setConstitution
     * Descricao: Atribui um valor de constituicao
     * Entrada: (int) constituicao
     * Saida: (void)
    */
    public void setConstitution(int constitution)
    {
        if(constitution >= 1 && constitution < 100)
            this.constitution = constitution;
    }

    /*
     * Nome: equipItem
     * Descricao: Coloca um item no inventario
     * Entrada: (Item) item
     * Saida: (void)
    */
    public void equipItem(Item item)
    {
        myitems.insertItem(item);
    }

    /*
     * Nome: unequipItem
     * Descricao: Tira um item no inventario
     * Entrada: (Item) item
     * Saida: (void)
    */
    public void unequipItem(Item item)
    {
        myitems.removeItem(item.getName());
    }
}
